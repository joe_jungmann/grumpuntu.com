/**
 * Store
 */
var _,
    store,
    sockets,
    events,
    diff = require('deep-diff').diff;



var updateProviders;

/**
 * [dispatchChanges description]
 * @param  {[type]}
 * @param  {[type]}
 * @param  {[type]}
 */
var dispatchChanges;

var handlers = {
    'store-get': function(socket) {
        socket.emit('store-update', store);
        updateProviders();
    },
    'store-set': function(item, value) {
        var current = store[item];
        var difference = diff(current, value);
        if (!difference) {
            console.log('no difference.');
            return;
        }

        store[item] = value;
        dispatchChanges(null, item, difference);
    }
};

/**
 * [registerHandlers description]
 * @param  {[type]}
 */
var registerHandlers = function(events) {
    _.forEach(handlers, function(action, name) {
        events.on(name, action);
    });
};

/**
 * Component annotations.
 */
exports['@singleton'] = true;

var Store = function(sockets) {

    updateProviders = _.debounce(function() {
        events.emit('provider-update');
    }, 5000);
    updateProviders();
    events.emit('provider-update');
    setInterval(function() {
        console.log('provider update');
        updateProviders();
    }, 10000);


    registerHandlers(events);
    return {
        get: function() {
            return store;
        }
    };
};

exports = module.exports = function(settings, preferences, sockets) {
    _ = Grump._;
    events = Grump.emitter;
    store = _.merge(preferences.get(), {});
    dispatchChanges = function(socket, parent) {
        socket = socket || sockets;
        socket.emit('store-update', store[parent], parent);
    };

    return new Store();
};
exports['@require'] = ['settings', 'preferences', 'sockets'];
