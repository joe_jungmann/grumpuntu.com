/**
 * Plex
 */


/**
 * Imports
 */
var PlexAPI = require('plex-api'),
    _ = require('lodash'),
    BaseProvider = require('../base'),
    client = new PlexAPI('www.grumpuntu.com');



 /*
        ---Shows---
  */

var allShows = [],
    getUrl = function(id) {
    return `http://www.grumpuntu.com:32400/web/index.html#!/server/31571ba474e55ffb53c643eefb111aced711ca96/details/%2Flibrary%2Fmetadata%2F${id}`;
};


/**
 * [addChildren description]
 * @param {[type]}
 * @param {[type]}
 * @param {[type]}
 */
var addChildren = function(item, callBack, err) {
    item.children = [];

    client.find(`/library/metadata/${item.ratingKey}/children`).then(function(dir) {
        dir.forEach(function(i) {
            var current = i.attributes;
            item.children.push(current);
            addChildren(current);
        });

    }, err);
};

// list all shows
var makeShowList = function(callBack, err) {
    client.find('/library/sections/1/all').then(function(dir) {
        dir.forEach(function(i) {
            var show = i.attributes;
            allShows.push(show);
            addChildren(show);
        });
    }, err);
};


var getShowByName = function(name) {
    var item = _.where(allShows, {
        title: name
    });
    return item;
};

var getEpisode = function(name, episode) {
    //console.log('name'+name+' eppy::'+episode)
    var item = _.where(allShows, {
        title: name
    })[0];

    if (!item) {
        return 0;
    }

    var anItem;
    item.seasons.forEach(function(s) {
        var found = _.where(s.episodes, {
            title: episode
        });

        if (found && found.length > 0) {
            anItem = found[0];
            anItem.url = getUrl(found[0].ratingKey);

        }
    });

    return anItem;
};

/**
 * [getShowing description]
 * @param  {Function}
 * @return {[type]}
 */
var getShowing = function() {
    return new Promise(function(resolve, reject) {
        client.query('/status/sessions').then(function(dir) {
            resolve(dir.video);
        });
    });
};


exports.getSessions = getShowing;

var allMovies = [];

var getMovie = function(name) {

    var found = _.where(allMovies, {
        title: name
    });

    var item = null;
    if (found && found.length > 0) {
        item = found[0];
        item.url = getUrl(found[0].ratingKey);
    }

    return item;
};


var getMovies = function() {
    return new Promise(function(resolve, reject) {


        client.find('/library/sections/2/all', {
            type: 'movie'
        }).then(function(directories) {
            directories.forEach(function(movie) {
                allMovies.push(movie.attributes);
            });
            resolve(allMovies);
            // directories would be an array of sections whose type are "movie"
        }, function() {
            throw new Error('Could not connect to server');
        });
    });
};


module.exports.getMovies = getMovies;



var handlers = {
    connect: function(socket) {
        console.log('get sessions');
        getShowing().then(function(item) {
            socket.emit('plex-playing', item);
        });
    },
    playShow: function(socket, msg) {
        var episode;
        try {
             episode = getEpisode(msg.show, msg.episode);
        } catch (e) {
            console.log('Err:' + e);
            console.log('for item::::::' + msg);
            return;
        }

        socket.emit('plex-url', episode);
    },
    playMovie: function(socket, msg) {
        var episode = getMovie(msg);
        socket.emit('plex-url', episode);
    }
};






var definition = [{
    source: 'Plex',
    provides: 'plex',
    'fake':function(){},
    'handlers': handlers
}];

module.exports = function() {

    makeShowList();
    getMovies();


    return BaseProvider.apply(this, Array.prototype.slice.call(arguments).concat(definition));
};
