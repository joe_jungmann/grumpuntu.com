/**
 * Grump Sockets
 */

/**
 * [Sockets description]
 */
function Sockets() {

    return {
        bindings: [],
        on: function(path) {
            console.log('binding!');
            this.bindings.push(path);
        },
        emit: function(a, b) {
            console.log('socket emit:');
            console.log(b);
        }
    };
}

/**
 * [exports description]
 * @return {[type]} [description]
 */
exports = module.exports = function() {
    global.sockets= new Sockets();
    return global.sockets;
};


/**
 * Component annotations.
 */
exports['@singleton'] = true;
