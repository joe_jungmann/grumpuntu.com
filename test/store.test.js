// jshint ignore:start
var assert = require("assert"),
    _ = require('lodash');

describe('Store', function() {
    var IoC = require('./supports'),
        store,
        Grump;
    global.setInterval = function() {
        // clear
    };
    
    beforeEach(function(done) {
        var i = IoC();
        Grump = global.Grump;
        store = i.create('store');
        done();
    });

    afterEach(function(done) {
        store = null;
        done();
    });

    describe('Bindings', function() {
        it('should have two bindings', function(done) {
            assert.equal(_.toArray(Grump.emitter._events).length, 2);
            done();
        });

        it('should still have two bindings', function(done) {
            assert.equal(_.toArray(Grump.emitter._events).length, 2);
            done();
        });

    });
});
