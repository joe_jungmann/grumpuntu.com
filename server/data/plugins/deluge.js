/**
 * Deluge
 */

/**
 * Imports 
 */
var request = require('request'),
    BaseProvider = require('../base'),
    _ = Grump._,
    faker = require('faker'),
    baseUrl = 'http://www.grumpuntu.com:8112/json';

var makeFake = function() {
    var future = new Date();
    future.setHours(future.getHours()+(faker.helpers.randomNumber(5)+1));

    return {
        id: 1,
        name: faker.hacker.adjective()+' ' + faker.hacker.verb(),
        progress: faker.helpers.randomNumber(100),
        'eta': (future-Date.now())/1000/60
    };
};

var api = function(method, params) {
    params = params || [];
    var parameters = {
        json: true,
        jar: true,
        gzip: true,
        body: {
            'id': 1,
            'method': method,
            'params': params
        }
    };

    return new Promise(function(resolve, reject) {
        request.post(baseUrl, parameters, function(err, response) {
            if (err) {
                reject();
            }
            resolve(response);
        });
    });
};


var connect = function() {
        return api('web.connected');
    },
    authenticate = function() {
        return api('auth.login', ['deluge']);
    },
    listTorrents = function() {
        return api('web.update_ui', [
            ['name', 'progress','download_payload_rate','eta'], {
                'state': 'Downloading'
            }
        ]);
    };

var getItems = function() {
    return new Promise(function(resolve,reject) {
        authenticate().then(connect,reject).then(listTorrents).then(function(response) {
            var torrents = response.toJSON().body.result.torrents;
            resolve(_.toArray(torrents));
        });
    });
};

var definition = [{
    source: 'Deluge',
    provides: 'torrents',
    'getItems': getItems,
    'fake': makeFake,
    map:{
        'name':'name',
        'progress':  {
            key: 'progress',
            transform: function(v) {
                return Math.round(v*100)/100;
            }
        },
        'eta':'eta',
        'download_payload_rate':{
            key: 'download_payload_rate',
            transform: function(v) {
                if(v===0){
                    return 0;
                }
                
                return ( Math.round((v/1024)*100)/100)+'KBps';
            }
        }
    }
}];

module.exports = function() {
    return BaseProvider.apply(this, Array.prototype.slice.call(arguments).concat(definition));
};
