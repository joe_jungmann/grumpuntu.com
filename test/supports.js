// jshint ignore:start
var events =  require('events').EventEmitter;
global.Grump= {_: require('lodash')};
module.exports = function() {
    var IoC = require('electrolyte');
    Grump.emitter= new events();

    var node1 = IoC.node('test/components')
    var node2 = IoC.node('server/components');
    IoC.loader(node1);
    IoC.loader(node2);
    return IoC;
};
