/**
 * Grump Server
 *
 * 
 */
var IoC = require('electrolyte');

/**
 * Register Components Source
 */
IoC.loader(IoC.node('server/components'));

/**
 * [Grump description]
 * @type {Object}
 */
global.Grump = {
	emitter: new (require('events').EventEmitter)(),
	_ : require('lodash')
};

Grump.store = IoC.create('store');
Grump.plugins = require('./data/index')();


module.exports = Grump;