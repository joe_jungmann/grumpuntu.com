/* global window,io,document,App,Polymer */
var GrumpApp = function() {

    /**
     * [Grump description]
     * @type {Object}
     */
    var Grump = function() {
        var destination = window.location.hostname.split(':')[0];
        if (destination.indexOf('.com') < 0) {
            destination = 'localhost';
        }



        var socket = io.connect('http://' + destination + ':3001');

        return {
            page: function(page) {
                this.pages = this.pages || document.querySelector('core-animated-pages');
                
                if (page < 0 || page > this.pages.children.length - 1) {
                    return;
                }

                this.pages.selected = page;
            },
            init: function() {
                this.on('plex-url', this.open);
            },
            baseUrl: 'http://www.grumpuntu.com/',
            on: function() {
                return socket.on.apply(socket, arguments);
            },
            emit: function() {
                return socket.emit.apply(socket, arguments);
            },
            open: function(item) {
                if (!item) {
                    return;
                }

                window.open(item.url);
            }
        };
    };

    return new Grump();
};

window.App = new GrumpApp();

window.playMovie = function(item) {
    App.emit('playMovie', item.name);
};


window.selectShow = function(item) {
    App.viewer.setData(item);
};
