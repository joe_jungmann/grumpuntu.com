/**
 * Grump Plugin Base
 */


/**
 * Imports
 */
var _ = global.Grump._,
    merge = require('object-mapper').merge;

var mapData = function(items, map) {
    return _.map(items, function(i) {
        return merge(i, {}, map);
    });
};

var bindHandlers = function(emitter, handlers) {
    _.forEach(handlers, function(action, name) {
        emitter.on(name, action);
    });
};

/**
 * [Base description]
 * @param {[type]}
 * @param {[type]}
 */
function Base(emitter, _p) {
    _p.status = 'unknown';

    var emit = function() {
        _p.getItems().then(function(data) {
            if (_p.map) {
                data = mapData(data, _p.map);
            }

            emitter.emit('store-set', _p.provides, data);
        }, function() {
            console.log('Error reaching provider: ' + _p.source);
            _p.status = 'unavailable';
            var data = [_p.fake(1), _p.fake(2)];
            emitter.emit('store-set', _p.provides, data);
        });
    };

    bindHandlers(emitter, _p.handlers);

    if (_p.getItems) {
        emitter.on('provider-update', emit);
    }

    return {
        provides: _p.provides,
        canUpdate: function() {
            return true;
        },
        status: function() {
            return _p.status;
        }
    };
}

module.exports = Base;
