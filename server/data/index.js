/**
 * Grump Plugin Registration
 */



/**
 * Imports 
 */
var requireDirectory = require('require-dir'),
	_ = Grump._;

/**
 * [plugins description]
 * @type {Array}
 */
var plugins = [];

/**
 * [register description]
 * @param  {[type]}
 * @return {[type]}
 */
var register = function(Plugin) {
    var plugin = new Plugin(Grump.emitter);

    console.log(`Loaded plugin for ${plugin.provides}`);
    plugins.push(plugin);
};

module.exports = function() {
    console.log('Loading plugins');

    _.forEach(requireDirectory('./plugins'), register);

    return {
    	'register': register
    };
};
