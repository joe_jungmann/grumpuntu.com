/**
 * Grump Sockets
 */

/**
 * [Sockets description]
 */
function Sockets() {
    var io = require('socket.io')(3001),
        emitter = Grump.emitter,
        middleware = require('socketio-wildcard')();

    io.use(middleware);

    io.on('connection', function(socket) {
        emitter.emit('connection', socket);

        console.log('connection');

        socket.on('disconnect', function() {
            console.log('user disconnected');
        });

        // delay sending message by 200ms
        //setTimeout(getContent, 200);
        socket.on('*', function(request) {
            var data = request.data;
            var method = data.splice(0, 1);
            data.push(socket);
            data.reverse();
            emitter.emit.apply(emitter, [method].concat(data));
        });
    });

    return io;
}

/**
 * [exports description]
 * @return {[type]} [description]
 */
exports = module.exports = function() {
    return new Sockets();
};


/**
 * Component annotations.
 */
exports['@singleton'] = true;