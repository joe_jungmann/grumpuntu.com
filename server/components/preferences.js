/**
 * Grump Preferences
 */


/**
 * Imports 
 */
var fs = require('fs'),
    defaults = 'grump-preferences.default',
    file = '.grump/preferences.json',
    preferences,
    write = function() {
        fs.writeFile(file, JSON.stringify(preferences));
    };


var initialize = function() {
    var initial = JSON.parse(fs.readFileSync(defaults, 'utf-8'));
    if (!fs.existsSync('.grump')) {
        fs.mkdirSync('.grump');
    }

    fs.writeFileSync(file, JSON.stringify(initial));
};

if (!fs.existsSync(file)) {
    initialize();
}


preferences = JSON.parse(fs.readFileSync(file, 'utf-8'));

/**
 * [get description]
 * @param  {[type]} item [description]
 * @return {[type]}      [description]
 */
exports.get = function(item) {
    if (arguments.length === 0) {
        return preferences;
    }

    return preferences[item];
};

/**
 * [set description]
 * @param {[type]} item  [description]
 * @param {[type]} value [description]
 */
exports.set = function(item, value) {
    preferences[item] = value;
    write(preferences);
};
