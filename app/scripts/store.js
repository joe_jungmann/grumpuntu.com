/* global App,_ */
(function() {

    App.emit('store-get');

    var __callbacks = {},
        __on = {},
        items = [];

    /**
     * [setStore description]
     * @param {[type]}
     * @param {[type]}
     */
    var setStore = function(data, parent) {
        if (parent) {
            console.log('dispatching: ' + parent);
            items[parent] = data;
            dispatch(parent);
            return;
        }

        items = data;

        _.forEach(items, function(value, property) {
            dispatch(property);
        });
    };

    /**
	 * [handleChange description]

	 * @param  {[type]}
	 * @param  {[type]}
	 * @return {[type]}
	 */
    var handleChange = function(changes, parent) {

        var current = items[parent];
        _.forEach(changes, function(c) {
            if (c.kind != 'E') {
                return;
            }

            current[c.path[0]][c.path[1]] = c.rhs;
        });
    };

    App.on('store-change', handleChange);
    App.on('store-update', setStore);

    /**
	 * [dispatch description]

	 * @param  {[type]}
	 * @return {[type]}
	 */
    var dispatch = function(property) {
        var calls = [].concat(__callbacks[property] || [], __on[property] || []);

        if (!calls) {
            return;
        }

        _.forEach(calls, function(callback) {
            callback(items[property]);
        });

        __callbacks[property] = [];
    };


    /**
     * [store description]
     * @type {Object}
     */
    var store = {
        get: function(item, callback) {
            var content = items[item];

            if (content) {
                callback(content);
                return;
            }

            __callbacks[item] = __callbacks[item] || [];
            __callbacks[item].push(callback);
        },
        on: function(item, callback) {
            var content = items[item];

            if (content) {
                callback(content);
            }

            __on[item] = __on[item] || [];
            __on[item].push(callback);
        }

    };

    App.store = store;
    App.init();
}());