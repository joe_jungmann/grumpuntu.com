/**
 * Sickbeard/Sickrage
 */

/**
 * Imports 
 */
var request = require('request'),
    BaseProvider = require('../base'),
    _ = Grump._,
    baseUrl ='http://www.grumpuntu.com:8081/api/0a059a208826829e54abbc9b739933a8/?cmd=history&type=downloaded',
    faker = require('faker');


var makeFakeSeason = function(x) {
    return {
        seasonId: x,
        episodes: [{
            name: 'Episode 1',
            status: 'Downloaded',
            airdate: Date.now()
        }, {
            name: 'Episode 2',
            status: 'Downloaded',
            airdate: Date.now()
        }]
    };
};

var makeFake = function(x) {
    return {
        _id: x,
        image: faker.image.image(),
        'name': faker.company.companyName(),
        seasons: makeFakeSeason(x),
    };
};

var getItems = function() {
    return new Promise(function(resolve, reject) {
        request(baseUrl,
            function(error, response) {
                if (error) {
                    reject();
                    return;
                }

                var data = JSON.parse(response.body).data;
                
                resolve(_.uniq(data,'show_name'));
            });
    });
};

var definition = [{
    source: 'Sickbeard',
    provides: 'tv',
    'getItems': getItems,
    'fake': makeFake,
    map: {
        'indexerid': '_id',
        'show_name': 'name',
        'tvdbid': {
            key: 'image',
            transform: function(v) {
                return `http://www.grumpuntu.com:8081/cache/images/thumbnails/${v}.poster.jpg`;
            }
        }
    }
}];

module.exports = function() {
    return BaseProvider.apply(this, Array.prototype.slice.call(arguments).concat(definition));
};
