/**
 * CouchPotato
 */


/**
 * Imports 
 */
var request = require('request'),
    BaseProvider = require('../base'),
    faker = require('faker'),
    baseUrl = 'http://www.grumpuntu.com:5050/api/807f8ade24d74b6881e1c996a125f5c4/movie.list?status=done';

var makeFake = function(x) {
    return {
        _id: x,
        image: faker.image.image(),
        'show_name': faker.company.companyName(),
        seasons: []
    };
};

var getItems = function() {
    return new Promise(function(resolve, reject) {
        request(baseUrl,
            function(error, response) {
                if (error) {
                    reject();
                    return;
                }

                resolve(JSON.parse(response.body).movies);
            });
    });
};


var definition = [{
    source: 'CouchPotato',
    provides: 'movies',
    'getItems': getItems,
    'fake': makeFake,
    map: {
        '_id': '_id',
        'title': 'name',
        'files.image_poster': {
            key: 'image',
            transform: function(v) {
                return v[0] && 'http://www.grumpuntu.com:5050/api/807f8ade24d74b6881e1c996a125f5c4/file.cache' + v[0].replace('/var/opt/couchpotato/cache', '');
            }
        }
    }
}];

module.exports = function() {
    return BaseProvider.apply(this, Array.prototype.slice.call(arguments).concat(definition));
};
